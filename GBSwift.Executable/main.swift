//
//  main.swift
//  GBSwift
//
//  Created by Ricardo Amores Hernández on 30/5/18.
//  Copyright © 2018 develovers. All rights reserved.
//
import Foundation
import GBSwift_Lib
import GBSwift_disassembler


//extension Sequence {
//
//    func groupBy<G: Hashable>(closure: (Iterator.Element)->G) -> [G: [Iterator.Element]] {
//        var results = [G: Array<Iterator.Element>]()
//
//        forEach {
//            let key = closure($0)
//
//            if var array = results[key] {
//                array.append($0)
//                results[key] = array
//            }
//            else {
//                results[key] = [$0]
//            }
//        }
//
//        return results
//    }
//}

func findResourceRecursive(_ name:String) -> [URL] {
    
    let directoryPath = Bundle.main.resourcePath!
    let directoryURL = URL(fileURLWithPath: directoryPath)
    
    let enumerator = FileManager.default.enumerator(atPath: directoryPath)
    var fileURLs: [URL] = []
    
    while let filePath = enumerator?.nextObject() as? String {
        
        let resourceURL = URL(fileURLWithPath: filePath)
        if resourceURL.lastPathComponent == name {
            fileURLs.append(directoryURL.appendingPathComponent(filePath))
        }
    }
    return fileURLs
}

class Debugger : CPUHardwareEventDelegate {
    
    func onInstructionWillExecute(cpu: CPU, opcode: OpCode) {
        
        var machineCode:Array<Word> = []
        var idx:DWord = 0
        let pc = cpu.registers.PC-1
        while(opcode.length > 1 && idx < opcode.length) {
            machineCode.append(cpu.read(fromAddress: pc + idx))
            idx += 1
        }

        print(Instruction(position: UInt32(pc), machineCode:machineCode[0..<machineCode.count], opcode: opcode))
    }
    
    func onInstructionDidExecute(cpu: CPU, opcode: OpCode, took cycles: OpCode.CyclesCount) {
//        print(opcode, cycles)
//        print(cpu.registers)
//        cpu.pause()
    }
    
    func onInterruptTriggered(cpu: CPU, interrupt: InterruptController.Interrupt, jumpVector:Address) {
        print("Interrupt !: \(interrupt) jumping to \(jumpVector.asHex())")
    }
}


// MARK: - START program
var cpu = CPU()

let data = Array.init(try! Data(contentsOf: findResourceRecursive("01-special.gb").first!))
let c = Cartdridge(withROMData: ROM(withContents: data), andSRAMData: nil)



let disassembledRom = DisassembledROM(withROMData: data, andDisassembler: LR35902_Disassembler())

//cpu.setHardwareEventDelegate(delegate: Debugger())

cpu.loadCartdridge(c)
cpu.run()
