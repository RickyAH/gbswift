//
//  Extensions.swift
//  GBSwift.OpcodeGenerator
//
//  Created by Ricardo Amores Hernández on 01/11/2018.
//  Copyright © 2018 develovers. All rights reserved.
//

import Foundation

extension Collection {
    /// Finds such index N that predicate is true for all elements up to
    /// but not including the index N, and is false for all elements
    /// starting with index N.
    /// Behavior is undefined if there is no such N.
    func binarySearch(predicate: (Iterator.Element) -> Bool) -> Index {
        var low = startIndex
        var high = endIndex
        while low != high {
            let mid = index(low, offsetBy: distance(from: low, to: high)/2)
            if predicate(self[mid]) {
                low = index(after: mid)
            } else {
                high = mid
            }
        }
        return low
    }
}

extension UnsignedInteger {
    public func asHex() -> String {
        return String(format:"%06X", self as! CVarArg)
    }
}

