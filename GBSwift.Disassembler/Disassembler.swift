//
//  File.swift
//  GBSwift.Lib
//
//  Created by Ricardo Amores Hernández on 16/8/18.
//  Copyright © 2018 develovers. All rights reserved.
//

import Foundation
import GBSwift_Lib

public struct Instruction : CustomStringConvertible {
    public typealias MachineCode = ArraySlice<Word>
    
    public let position:UInt32
    public let machineCode:String
    public let mnemonic:String
    public let arguments:String
    
    public init(position:UInt32, machineCode:MachineCode, opcode:OpCode) {
        let machineCodeStr = machineCode.map{ $0.asHex() }

        self.position = position
        self.machineCode = machineCodeStr.joined(separator: " ")
        
        if (opcode.length > 1) {
            let argumentsRange = 1...(Int(opcode.length) - 1)
            self.arguments = machineCodeStr[argumentsRange].reversed().joined()
        } else {
            self.arguments = ""
        }
        self.mnemonic = Instruction.createMnemonic(opcode.mnemonic, arguments)
    }
    
    static func createMnemonic(_ mnemonic:String, _ arguments: String) -> String {
        return mnemonic.replacingOccurrences(of: "a16", with:arguments)
                .replacingOccurrences(of: "a8", with: arguments)
                .replacingOccurrences(of: "d16", with: arguments)
                .replacingOccurrences(of: "d8", with: arguments)
                .replacingOccurrences(of: "r8", with: arguments)
    }
    
    public var description:String {
        
        get { return "\(position.asHex())\t\(machineCode.padding(toLength: 8, withPad: " ", startingAt: 0))\t\(mnemonic)" }
    }
}

/// Defines a protocol for a CPU disassembler
public protocol Disassambler {
    typealias MachineCodeData = Dictionary<UInt32, Instruction>
    
    /// Disassembles a stream of bytes from a ROM
    ///
    /// - Parameter data: ROM data to disassemble
    /// - Returns: A map of addresses inside the ROM that mark the beginning of an instruction and the bytes that
    /// contains the instruction (opcode + arguments)
    func disassemble(data:[Word], dataRanges:[ClosedRange<Int>]?) -> MachineCodeData
}

/// Struct to hold data for a disassembled ROM
public struct DisassembledROM {
    let assembledROM:[UInt8]!
    let disassembledROM:Disassambler.MachineCodeData!
    
    public init(withROMData ROMData:[UInt8], andDisassembler disassembler:Disassambler) {
        assembledROM = ROMData
        disassembledROM = disassembler.disassemble(data: ROMData, dataRanges:[ROM.HeaderRange])
    }
    
    public func getInstructions(inRange range: ClosedRange<UInt32>) -> [Instruction] {
        var lowerRomRangeIndex = range.lowerBound
        var upperRomRangeIndex = range.upperBound
        
        while lowerRomRangeIndex >= 0 && !self.disassembledROM.keys.contains(lowerRomRangeIndex) {
            lowerRomRangeIndex -= 1
        }
        
        while upperRomRangeIndex < disassembledROM.count && !self.disassembledROM.keys.contains(upperRomRangeIndex) {
            upperRomRangeIndex += 1
        }
        
        let finalRange = lowerRomRangeIndex...upperRomRangeIndex
        
        let filtered = self.disassembledROM.filter({ (element) -> Bool in
            let (position, _) = element
            return finalRange.contains(position)
        })
        
        let sorted = filtered.sorted { (first, second) -> Bool in
            first.key < second.key
        }
        
        return sorted.map {$0.value}
    }
}


/// Disassembler for GameBoy CPU instruction set
 
public struct LR35902_Disassembler : Disassambler {
    let opcodes:[OpCode]
    let extendedOpcodes:[OpCode]
    
    static var ROMHeaderRange:ClosedRange<Int> {
        get { return ROM.HeaderRange }
    }
    
    public init() {
        self.opcodes = LR35902_Opcodes
        self.extendedOpcodes = LR35902_Extended_Opcodes
    }
    
    public func disassemble(data:[Word], dataRanges:[ClosedRange<Int>]? = nil) -> MachineCodeData {
        var currentOpcodePosition = 0
        var disassembledMachineCode:MachineCodeData = [:]
        
        while currentOpcodePosition < data.count {
            
            for range in dataRanges ?? [] {
                if range.contains(currentOpcodePosition) {
                    currentOpcodePosition += 1
                    continue
                }
            }

            let opcode = decodeOpcode(data[currentOpcodePosition])
            if (opcode.length <= 0) {
                
                print("Invalid opcode: \(opcode) at position \(String(format:"%08X", currentOpcodePosition)) found when disassembling")
                currentOpcodePosition += 1
                continue
            }
            let machineCode = data[currentOpcodePosition...Int(currentOpcodePosition + Int(opcode.length) - 1)]
            let position = UInt32(currentOpcodePosition)
            let instruction = Instruction.init(position: position, machineCode: machineCode, opcode: opcode)
            
            disassembledMachineCode[position] = instruction
            currentOpcodePosition += Int(opcode.length)
        }
        
        return disassembledMachineCode
    }
    
    func decodeOpcode(_ code:Word) -> OpCode {
        if (code == 0xCB) {
            return extendedOpcodes[Int(code)]
        }
        
        return opcodes[Int(code)]
    }

    
}
