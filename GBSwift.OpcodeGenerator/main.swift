//
//  AppDelegate.swift
//  GBSwift.OpcodeGenerator
//
//  Created by Ricardo Amores Hernández on 2/6/18.
//  Copyright © 2018 develovers. All rights reserved.
//

import Foundation
import SwiftSoup
import GBSwift_Lib
import PathKit


struct Paths {
	public static var root = Path(#file + "../../../").normalize()
	public static var project = (Paths.root + Path("GBSwift.xcodeproj")).normalize()
	public static var GBSwiftLib = (Paths.root + Path("GBSwift.Lib")).normalize()
	public static var GBOpcodeGenerator = (Paths.root + Path("GBSwift.OpcodeGenerator")).normalize()
	public static var GBOpcodesHTMLFile = (Paths.GBOpcodeGenerator + Path("Gameboy_OPCODES.html")).normalize()
}

public enum OpType {
    case Misc_Control
    case Jumps_Calls
    case Load_Store_Move_8_Bit
    case Load_Store_Move_16_Bit
    case Arithmetic_Logical_8_Bit
    case Arithmetic_Logical_16_Bit
    case Rotation_Shift_8_Bit
    case Unknown
    
}

struct OpCodeDefinition: CustomDebugStringConvertible {
    public let code:UInt8
    public let length:UInt8
    public let cycles:UInt8
    public let cyclesOnJump:UInt8?
    public let mnemonic:String
    public let type:OpType
    public let flags:String
    
    public static var empty: OpCodeDefinition {
        get {
            return OpCodeDefinition(code: 0,
                          length: 0,
                          cycles: 0,
                          cyclesOnJump: nil,
                          mnemonic: "<nil>",
                          type: OpType.Unknown,
                          flags: "")
        }
    }
    
    public var debugDescription:String {
        get {
			if (cyclesOnJump != nil) {
                return "[\(code)] \(mnemonic) - length: \(length) - cycles: \(cycles)/\(String(describing: cyclesOnJump))"
			} else {
				return "[\(code)] \(mnemonic) - length: \(length) - cycles: \(cycles)"
			}

        }
    }
}

func getTypeFromBgColor(_ colorString:String) -> OpType {
    switch colorString {
    case "#ff99cc":
        return OpType.Misc_Control
    case "#ffcc99":
        return OpType.Jumps_Calls
    case "#ffff99":
        return OpType.Arithmetic_Logical_8_Bit
    case "#ffcccc":
        return OpType.Arithmetic_Logical_16_Bit
    case "#ccccff":
        return OpType.Load_Store_Move_8_Bit
    case "#ccffcc":
        return OpType.Load_Store_Move_16_Bit
    case "#80ffff":
        return OpType.Rotation_Shift_8_Bit
        
    default:
        return OpType.Unknown
    }
}

func readTableFromHTMLFile(filepath:Path) -> Element {

	// get the contents as string
	let data = try! filepath.read()

	let html_raw = String(data: data, encoding: .ascii)

	let doc: Document = try! SwiftSoup.parse(html_raw!)

	return try! doc.select("tbody").first()!
}

func readTableFromHTMLFile2(filepath:Path) -> Element {
    
    // get the contents as string
    let data = try! filepath.read()
    
    let html_raw = String(data: data, encoding: .ascii)
    
    let doc: Document = try! SwiftSoup.parse(html_raw!)
    
    return try! doc.select("tbody").get(1)
}

func extractOpcode(_ cell:Element, _ instructionCode:UInt8) -> OpCodeDefinition? {
    let textNodes = cell.textNodes()
    if textNodes.count != 3 { return nil }
    
    var opName:String
    var opLength:UInt8
    var opDuration:UInt8
    var opDurationOnJump:UInt8?
    var opType:OpType
    var flags:String
    
    let splittedString = textNodes[1].text().components(separatedBy: "\u{00A0}").filter { !$0.isEmpty }
    opName = textNodes[0].text()
    opLength =  UInt8(splittedString[0])!
    opType = getTypeFromBgColor(try! cell.attr("bgcolor"))
    flags = textNodes[2].text().trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
    
    if splittedString[1].contains("/") {
        let maxMinCyclesStrings = splittedString[1].split(separator: "/", maxSplits: 2, omittingEmptySubsequences: true)
        opDuration = UInt8(maxMinCyclesStrings[0])!
        opDurationOnJump = UInt8(maxMinCyclesStrings[1])!
    } else {
        opDuration = UInt8(splittedString[1])!
        opDurationOnJump = nil
    }
    
    return OpCodeDefinition(code: instructionCode,
                  length: opLength,
                  cycles: opDuration,
                  cyclesOnJump: opDurationOnJump,
                  mnemonic: opName,
                  type: opType,
                  flags: flags)
}


func createOpcodes(table:Element) -> [OpCodeDefinition?] {
	var gbOpcodes = Array<OpCodeDefinition?>(repeating: nil, count: Int(UInt8.max)+1)

	var instructionCode = -1
	for row in try! table.select("tr:gt(0)") {
		for cell in try! row.select("td:gt(0) ") {
			instructionCode += 1
			let opCode = extractOpcode(cell, UInt8(instructionCode))
			gbOpcodes[instructionCode] = opCode
		}
	}

	return gbOpcodes

}

func writeOpcodesToFile(path: Path, opcodes:[OpCodeDefinition?]) {
    let flagsTemplate = """
// Zero:%@ Neg:%@ Half-Carry:%@ Carry:%@
"""
	let opcodeTemplate = """
OpCode(code: 0x%02X, mnemonic: "%@")  { (cpu: CPU) in
    %@
	return (length: %d, cycles: %@)
},
"""

	let formatter = DateFormatter()

	formatter.dateFormat = "dd-MM-yyyy"
	var fileString = "// AUTOGENERATED " + formatter.string(from:Date()) + "\n"

	fileString += "var autogenerated_LR35902_Opcodes = [\n"

	for maybeOpcode in opcodes {
        
        guard let opcode = maybeOpcode else { continue }

		var cyclesStr = ""
		if (opcode.cyclesOnJump != nil) {
			cyclesStr = String(format: "%d/%d", opcode.cycles, opcode.cyclesOnJump!)
		} else {
			cyclesStr = String(format: "%d", opcode.cycles)
		}
        
        let theFlags = opcode.flags.split(separator: " ")
        let flagsStr = String(format: flagsTemplate, theFlags[0] as CVarArg, theFlags[1] as CVarArg, theFlags[2] as CVarArg, theFlags[3] as CVarArg)
		fileString = fileString + String(format: opcodeTemplate, opcode.code, opcode.mnemonic, flagsStr, opcode.length, cyclesStr)
		fileString = fileString + "\n"


		print(opcode)
	}

	fileString += "]\n"
	try! path.write(fileString)

}

// MARK: main
writeOpcodesToFile(path: Path("~/Desktop/gb_cpu_opcodes.swift"), opcodes: createOpcodes(table: readTableFromHTMLFile(filepath: Paths.GBOpcodesHTMLFile)))
writeOpcodesToFile(path: Path("~/Desktop/gb_cpu_opcodes_prefix.swift"), opcodes: createOpcodes(table: readTableFromHTMLFile2(filepath: Paths.GBOpcodesHTMLFile)))



