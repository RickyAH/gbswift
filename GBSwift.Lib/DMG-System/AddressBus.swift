//
//  Memory.swift
//  GBSwift
//
//  Created by Ricardo Amores Hernández on 31/5/18.
//  Copyright © 2018 develovers. All rights reserved.

// Defines some common Address ranges in the gameboy hardware
struct AddressRanges {
    struct GB {
        static let IRQ_Vector           = AddressRange(0x0000..<0x00FF)
        static let ROM                  = AddressRange(0x0000..<0x8000)
        static let VRAM                 = AddressRange(0x8000..<0xA000)
        static let SRAM                 = AddressRange(0xA000..<0xC000)
        static let RAM                  = AddressRange(0xC000..<0xE000)
        static let Echo_RAM             = AddressRange(0xE000..<0xFE00)
        static let OAM_RAM              = AddressRange(0xFE00..<0xFEA0)
        static let Unusable             = AddressRange(0xFEA0..<0xFF00)
        static let Zero_Page            = AddressRange(0xFF00...0xFFFF)
        static let IO_Registers         = AddressRange(0xFF00..<0xFF80)
        static let IO_Registers_Video   = AddressRange(0xFF40...0xFF4B)
        static let High_RAM             = AddressRange(0xFF80..<0xFFFF)
        static let IF_InterruptFlag     = AddressRange(0xFF0F...0xFF0F)
        static let IE_InterruptEnable   = AddressRange(0xFFFF...0xFFFF)
    }
}

// defines a device that can be read/written with an index based access
public protocol SequencialAccessDevice {
    func read(atIndex: Int) -> Word
    func write(atIndex: Int, value: Word)
    var count:Int {get}
}

class AddressBus {
    
    // defines a map between an Address in the AddressBus space, and a pair or read/write operations for that address
    struct AddressIOMapping {
        typealias WriteFunc = (Word)->Void
        typealias ReadFunc = ()->Word
        
        let writeFc:WriteFunc
        let readFc:ReadFunc
        
        init(readFunction:@escaping ReadFunc, writeFunction:@escaping WriteFunc) {
            self.readFc = readFunction
            self.writeFc = writeFunction
        }
        
        func read() -> Word {
            return readFc()
        }
        
        func write(_ value:Word) {
            writeFc(value)
        }
    }
    
    // This data structure saves useful information what is assigned to a range of addresses in the complete address map
    struct AddressIOMappingInfo : CustomStringConvertible {
        let range: AddressRange
        let device: SequencialAccessDevice
        let name:String
        
        public init(range: AddressRange, device: SequencialAccessDevice, name: String) {
            self.range = range
            self.device = device
            self.name = name
        }
        
        public var description:String { return "\(range.description) - \(name)" }
    }

    var addressMap: [AddressIOMapping] = []
    var addressesRangeInfo: [AddressIOMappingInfo] = []
    
    init() {
        for _ in 0...Address.max {
            addressMap.append(AddressIOMapping(readFunction: { return 0xFF }, writeFunction: { _ in } ) )
        }
    }
    
    public func registerDevice(_ device:SequencialAccessDevice,
                               onAddressRange addressRange:AddressRange,
                               withName name:String) {
        assert(addressRange.count <= device.count)

        var idx:Int = 0
        for address in addressRange.lowerBound...addressRange.upperBound {
            let index = idx
            addressMap[Int(address)] = AddressIOMapping(readFunction: { return device.read(atIndex: index) },
                                                       writeFunction: { value in device.write(atIndex: index, value: value)} )
            idx += 1
        }
        
        addressesRangeInfo.append(AddressIOMappingInfo(range: addressRange, device: device, name: name))
        addressesRangeInfo.sort() { $0.range < $1.range }
    }
    
    public func removeDevice(onAddressRange addressRange:AddressRange) {
        for address in addressRange.lowerBound...addressRange.upperBound {
            addressMap[Int(address)] = AddressIOMapping(readFunction: { return 0xFF }, writeFunction: { _ in } )
        }

        if let idx = addressesRangeInfo.firstIndex(where: {$0.range == addressRange}) {
            addressesRangeInfo.remove(at: idx)
        }
    }
    
    public func reset() {
        addressMap.removeAll()
        addressesRangeInfo.removeAll()
    }
    
    public func read(fromAddress: Address) -> Word {
        return addressMap.withUnsafeBufferPointer(){ ptrToAddressMap in
            return ptrToAddressMap[Int(fromAddress)].read()
        }
    }
    
    public func write(toAddress: Address, value: Word) {
        addressMap.withUnsafeMutableBufferPointer() { ptrToAddressMap in
            ptrToAddressMap[Int(toAddress)].write(value)
        }
    }
}

extension AddressBus : CustomDebugStringConvertible {
    public var debugDescription:String {
        var description = ""
        for addressMap in addressesRangeInfo {
            description += "\n \(addressMap.name.padding(toLength: 25, withPad: " ", startingAt: 0))"
                + " [\(addressMap.range.lowerBound.asHex()), \(addressMap.range.upperBound.asHex())]"
        }
        return description
    }
}
