//
//  OpCode.swift
//  GBSwift
//
//  Created by Ricardo Amores Hernández on 3/6/18.
//  Copyright © 2018 develovers. All rights reserved.
//


public class OpCode : CustomStringConvertible, CustomDebugStringConvertible {
    public typealias CyclesCount = UInt8
    public typealias OpCodeLength = UInt8
    public typealias InstructionFunc = (CPU) -> CyclesCount 
    
    public let mnemonic:String          // 16 bytes
    public let execute:InstructionFunc  // 16 bytes
    public let code:UInt8               // 1 byte
    public let length:OpCodeLength      // 1 byte
    
    public init(code:UInt8,
                length:OpCodeLength,
                mnemonic:String,
                instruction: @escaping InstructionFunc) {
        self.code = code
        self.length = length
        self.execute = instruction
        self.mnemonic = mnemonic
    }

    public var description:String {
        get { return "[\(self.code.asHex())] \(self.mnemonic)" }
    }
    
    public var debugDescription:String {
        get { return "[\(self.code.asHex())] \(self.mnemonic)" }
    }
    
    public static var unimplemented: OpCode {
        get {
            return OpCode(code: 0, length: 0, mnemonic: "<nil>") { (cpu: CPU) -> CyclesCount in
                fatalError("Unimplemented opcode executed")
            }
        }
    }
}

