//
//  ROM.swift
//  GBSwift.Lib
//
//  Created by Ricardo Amores Hernández on 6/8/18.
//  Copyright © 2018 develovers. All rights reserved.
//

public class ROM : RAM {
    public static let HeaderRange = 0x0100...0x014F
    
    override public func write(atIndex index: Int, value: Word) {
        print("ERROR trying to write to ROM at index \(index) with the value \(value.asHex())")
    }
}
