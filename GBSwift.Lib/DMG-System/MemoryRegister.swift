//
//  MemoryMappedRegister.swift
//  GBSwift.Lib
//
//  Created by Ricardo Amores Hernández on 13/8/18.
//  Copyright © 2018 develovers. All rights reserved.
//

class HardwareRegister : SequencialAccessDevice {
    var count: Int { get { return 1 } }
    
    
    private let accesibleBitsBitMask:Word
    private let defaultValueBitMask:Word
    private var _value:Word = 0
    
    init(withInitialValue initialValue:Word, accesibleBitsBitMask:Word=0xFF, defaultValueBitMask:Word = 0xFF) {
        self.accesibleBitsBitMask = accesibleBitsBitMask
        self.defaultValueBitMask = defaultValueBitMask
        self.value = initialValue
    }
    
    public var value:Word {
        get {
            return _value
        }
        set(newValue) {
            let data = self.defaultValueBitMask | (newValue & self.accesibleBitsBitMask)
            _value = data
        }
    }
    
    public func setBit(_ bit:BitNumber) {
        value.setBit(bit)
    }
    
    public func resetBit(_ bit:BitNumber) {
        value.resetBit(bit)
    }
    
    public func getBit(_ bit:BitNumber) -> Bool {
        return value.getBit(bit)
    }
    
    
    func read(atIndex: Int) -> Word {
        assert(atIndex == 0)
        return self.value
    }
    
    func write(atIndex: Int, value: Word) {
        assert(atIndex == 0)
        
        self.value = value
    }
    

}

class MemoryMappedRegisters : SequencialAccessDevice {

    var count: Int { get { return registers.count } }
    
    let registers:[HardwareRegister]
    
    init(withCount count:Int, andInitialValue value:Word) {
        registers = Array<HardwareRegister>(repeating: HardwareRegister(withInitialValue: value),
                                            count: count)
    }
    
    func read(atIndex: Int) -> Word {
        return registers[atIndex].read(atIndex: 0)
    }
    
    func write(atIndex: Int, value: Word) {
        registers[atIndex].write(atIndex: 0, value: value)
    }
    
    
}
