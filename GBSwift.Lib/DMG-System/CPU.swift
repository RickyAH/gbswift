//
//  Instructions.swift
//  GBSwift.Lib
//
//  Created by Ricardo Amores Hernández on 3/6/18.
//  Copyright © 2018 develovers. All rights reserved.
//

import Foundation
import os.log
import os.signpost


public protocol CPUHardwareEventDelegate {
    func onInstructionWillExecute(cpu: CPU, opcode: OpCode)
    func onInstructionDidExecute(cpu: CPU, opcode: OpCode, took: OpCode.CyclesCount)
    func onInterruptTriggered(cpu: CPU, interrupt:InterruptController.Interrupt, jumpVector:Address)
}

public final class CPU : CPUHardwareEventDelegate {
    
    struct State {
        var isPaused = false
        var isRunning = false
        var cycles: Int64 = 0

    }
    
    struct BenchmarkInfo {
        var frameNumber:UInt64 = 0
        var cyclesInLastFrame:UInt64 = 0
    }
    
    public var registers = CPU.CPU_Registers()
    
    var state = CPU.State()
    var opcodes:[OpCode] = []
    var addressDecoder:AddressBus = AddressBus()
    var interruptController:InterruptController!
    var hardwareEventDelegate:CPUHardwareEventDelegate!
    
    public init() {
        self.opcodes = LR35902_Opcodes
        
        let ppu = PPU()
        self.interruptController = InterruptController(withCPU: self)

        addressDecoder.registerDevice(RAM(count: AddressRanges.GB.RAM.count),
                                      onAddressRange: AddressRanges.GB.RAM,
                                      withName: "Working RAM")

        addressDecoder.registerDevice(interruptController.interruptEnable,
                                      onAddressRange: AddressRanges.GB.IE_InterruptEnable,
                                      withName: "Interrupt Enable (IE)")

        addressDecoder.registerDevice(interruptController.interruptFlag,
                                      onAddressRange: AddressRanges.GB.IF_InterruptFlag,
                                      withName: "Interrupt Flag (IF)")

        addressDecoder.registerDevice(ppu.registers,
                                      onAddressRange: AddressRanges.GB.IO_Registers_Video,
                                      withName: "Video Registers")

        addressDecoder.registerDevice(ppu.videoRam,
                                      onAddressRange: AddressRanges.GB.VRAM,
                                      withName: "VRAM")

        addressDecoder.registerDevice(ppu.oamRam,
                                      onAddressRange: AddressRanges.GB.OAM_RAM,
                                      withName: "OAM RAM")
    }
    
    public func setHardwareEventDelegate(delegate:CPUHardwareEventDelegate) {
        self.hardwareEventDelegate = delegate
    }
    
    public func loadCartdridge( _ cartdridge:Cartdridge) {
        removeCartdridge()

        addressDecoder.registerDevice(cartdridge.ROMData,
                                      onAddressRange: AddressRanges.GB.ROM,
                                      withName: "ROM")
        
        
        if cartdridge.SRAMData != nil {
            addressDecoder.registerDevice(cartdridge.SRAMData!,
                                          onAddressRange: AddressRanges.GB.SRAM,
                                          withName: "SRAM")
        }
    }
    
    public func removeCartdridge() {
        addressDecoder.removeDevice(onAddressRange: AddressRanges.GB.ROM)
        addressDecoder.removeDevice(onAddressRange: AddressRanges.GB.SRAM)
    }
    
    public func run() {
        reset()
        state.isRunning = true;

        let general = OSLog(subsystem: "com.ricky.gbswift", category: "GB Emulator")

        let cyclesPerSecond:Int64 = 1048576
        
        let msPerFrame = 16.666666667
        var frameMs:Int64 = 0

        while(true) {
            
            os_signpost(.begin, log: general, name: "runFrame")
            let beforeFrameMs = Time.getTimeInMs();
            runFrame();
            os_signpost(.end, log: general, name: "runFrame")
            let afterFrameMs = Time.getTimeInMs();
            
            let msInEmulatorFrame = afterFrameMs - beforeFrameMs
            os_log("Frame executed in %fd msecs", msInEmulatorFrame)

            let sleepTime = msPerFrame - msInEmulatorFrame
            if (sleepTime > 0 ) {
                Thread.sleep(forTimeInterval: sleepTime / 1000)
            }
        }
        

    }
    
    public func runFrame() {
        
        let cyclesPerFrame:Int64 = 17556
        
        var frameCyclesCount:Int64 = 0
        while(state.isRunning) {
            if (state.isPaused) {
                continue
            }
            
            let previousCycles = state.cycles
            step();
            frameCyclesCount += state.cycles - previousCycles
            
            if frameCyclesCount >= cyclesPerFrame {
                return
            }
        }
    }
    
    public func step() {
        
        let interruptCycles = handleInterruptions()

        let code = fetch()
        
        let opcode = decode(instructionCode: code )
//        hardwareEventDelegate?.onInstructionWillExecute(cpu: self, opcode: opcode)
        
        let instructionCycles = opcode.execute(self)
        
//        hardwareEventDelegate?.onInstructionDidExecute(cpu: self, opcode: opcode, took: instructionCycles)
        
        // TODO: Count cycles
        state.cycles += Int64(instructionCycles + interruptCycles)
        
        // TODO: Update timers
 
    }
    
    public func pause() {
        state.isPaused = true
    }
    public func stop() {
        state.isRunning = false
    }
    
    public func resume() {
        state.isPaused = false
    }
    
    public func reset() {
        registers.reset()
        
        // TODO: give possibility to run the Nintendo Logo animation code
        registers.PC = 0x0100
    }
    
    
    public func read(fromAddress address:Address) -> Word {
        return addressDecoder.read(fromAddress: address)
    }
    
    public func write(toAddress address:Address, value:Word) {
        addressDecoder.write(toAddress: address, value: value)
    }
    
    func fetch() -> Word {

        let data = addressDecoder.read(fromAddress: registers.PC)
        registers.PC = registers.PC &+ 1

        return data
    }
    
    func decode(instructionCode: Word) -> OpCode {
        return opcodes.withUnsafeBufferPointer(){ return $0[Int(instructionCode)] }
    }

    /*
     Interrupts are checked before fetching a new instruction.
     If any IF flag and the corresponding IE flag are both '1' and IME is set to '1' too, the CPU will
     push the current PC into the stack, will jump to the  corresponding interrupt vector and set IME
     to '0'. If IME is '0', this won't happen.
     This flags are only cleared when the CPU jumps to an interrupt vector because of an interrupt
     or IF is written manually.
     
     If 2 or more interrupts are requested at the same time and the corresponding IE bits are set,
     the vector with lower address has higher priority (vertical blank has the highest priority, joypad
     the lowest priority).
     
     It takes 20 clocks to dispatch an interrupt. If CPU is in HALT mode, another extra 4 clocks are
     needed. This timings are the same in every Game Boy model or in double/single speeds in CGB/AGB/AGS.
     */
    func handleInterruptions() -> OpCode.CyclesCount {
        
        // TODO: take into account adding 4 cycles if we are in HALT mode
        var cycles:OpCode.CyclesCount = 0
        
        if let activeInterrupt = interruptController.findActiveInterrupt() {
            
            // Mark this interrupt as processed
            interruptController.deactivateInterrupt(activeInterrupt)
            
            // Disable interruptions while processing the interrupt
            interruptController.disableInterruptions()
            let interruptAddress = InterruptController.getJumpAddressFor(activeInterrupt)
            
            // Jump to interrupt vector
            self.PUSH_To_Stack(self.registers.PC)
            self.registers.PC = interruptAddress
            
            self.hardwareEventDelegate?.onInterruptTriggered(cpu: self,
                                                            interrupt: activeInterrupt,
                                                            jumpVector: interruptAddress)
        
            cycles = 20
            
            return cycles
        }
        
        // TODO: back from HALT/STOP states, even if we don't process the interruptions
        return cycles
    }

    // MARK: - CPUHardwareEventDelegate
    public func onInstructionWillExecute(cpu: CPU, opcode: OpCode){}
    public func onInstructionDidExecute(cpu: CPU, opcode: OpCode, took: OpCode.CyclesCount){}
    public func onInterruptTriggered(cpu: CPU, interrupt:InterruptController.Interrupt, jumpVector:Address){}
}
