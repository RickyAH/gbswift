//
//  PPU.swift
//  GBSwift.Lib
//
//  Created by Ricardo Amores Hernández on 04/11/2018.
//  Copyright © 2018 develovers. All rights reserved.
//


// This special class of RAM can be disabled by the PPU during two operations:
// - oam search: ppu access the oam memory to filter the subset of sprites visible
// - pixel transfer: ppu reads VRAM to generate the pixels to draw.

// When disabled, any writes do nothing, and any read returns 0xff

class VRAM : RAM {
    
    fileprivate var isDisabled:Bool = false
    
    convenience init() {
        self.init(withContents: Array.init(repeating: Word(), count: AddressRanges.GB.VRAM.count) )
    }
    
    override func read(atIndex: Int) -> Word {
        if (isDisabled) {
            return 0xFF
        }
        
        return super.read(atIndex: atIndex)
    }
    
    override func write(atIndex: Int, value: Word) {
        if (isDisabled) {
            return
        }
        
        super.write(atIndex:atIndex, value:value)
    }
}

class OAM_RAM : VRAM {
    
    convenience init() {
        self.init(withContents: Array.init(repeating: Word(), count: AddressRanges.GB.OAM_RAM.count) )
    }
}

final class PPU {
    
    enum VideoRegistersIndex : Int {
        case FF40_LCD_Control
        case FF41_LCD_STAT
        case FF42_ViewPort_Scroll_Y
        case FF43_ViewPort_Scroll_X
        case FF44_LY_Value
        case FF45_LYC_Value
        case FF46_DMA
        case FF47_BG_Palette
        case FF48_OBJ_Palette_0
        case FF49_OBJ_Palette_1
        case FF4A_Window_Position_Y
        case FF4B_Window_Position_X
    }
    
    public let videoRam:VRAM = VRAM()
    public let oamRam:OAM_RAM = OAM_RAM()
    public let registers = MemoryMappedRegisters(withCount: AddressRanges.GB.IO_Registers_Video.count,
                                                 andInitialValue: 0x00)
}
