//
//  CPU_LD_Helpers.swift
//  GBSwift.Lib
//
//  Created by Ricardo Amores Hernández on 10/6/18.
//  Copyright © 2018 develovers. All rights reserved.
//

extension CPU {
    
    func LD_ZeroPage(dest:inout Word, offset:Word) {
        let zeroPageAddress = AddressRanges.GB.Zero_Page.lowerBound + Address(offset)
        dest = read(fromAddress: zeroPageAddress)
    }

    func LD_ZeroPage(offset:Word, value:Word) {
        let zeroPageAddress = AddressRanges.GB.Zero_Page.lowerBound + Address(offset)
        write(toAddress: zeroPageAddress, value: value)
    }

    
    func Read_Word_From_PC() -> Word {
        let value = read(fromAddress: registers.PC)
        registers.PC += 1
        return value
    }
    
    func Read_DWord_From_PC() -> DWord {
        // GB is Little Endian
        // LD BC 0xFA02 means lsb comes first
        //      C <-- 0xFA
        //      B <-- 0X02
        
        let lowWord = Read_Word_From_PC()
        let highWord = Read_Word_From_PC()
        return DWord(highWord: highWord, lowWord: lowWord)
    }
    
    func LD_Register_Inmmediate(_ dest:inout Word) {
        dest = Read_Word_From_PC()
    }
    
    func LD_Register_Inmmediate(_ dest:inout DWord) {
        dest = Read_DWord_From_PC()
    }
    
    func INC_Register(_ dest:inout Word) {
        
        let previous = dest
        dest = dest &+ 1
        
        registers.F.toggleFlag(.Zero, when: dest == 0)
        registers.F.resetFlag(.Subtraction)
        
        let hasHalfCarry = hasAdditionSetHalfCarry(lhs: previous, rhs: 1)
        registers.F.toggleFlag(.HalfCarry, when: hasHalfCarry)


    }

    func INC_Register(_ dest:inout DWord) {
        dest = dest &+ 1
    }
    
    func DEC_Register(_ dest:inout Word) {
        
        let previous = dest
        dest = dest &- 1
        
        registers.F.toggleFlag(.Zero, when: dest == 0)
        registers.F.setFlag(.Subtraction)
        
        let hasHalfCarry = hasSubtractionSetHalfCarry(lhs:previous, rhs: 1)
        registers.F.toggleFlag(.HalfCarry, when: hasHalfCarry)


    }
    
    func DEC_Register(_ dest:inout DWord) {
        dest = dest &- 1
    }
    
    func ADD_Register(_ dest:inout Word, _ source: Word, withCarry:Bool = false ) {
        
        var carry:Word = 0
        
        if (withCarry && registers.F.isFlagSet(.Carry)) {
            carry = 1
        }
        let (partial, overflowCarry) = source.addingReportingOverflow(carry)
        let (result, overflowAdd) = dest.addingReportingOverflow(partial)
        
        let halfOverflow = hasAdditionSetHalfCarry(lhs:dest, rhs:partial)
    
        let overflowed = overflowAdd || overflowCarry
        
        registers.F.toggleFlag(.Zero, when: result == 0)
        registers.F.resetFlag(.Subtraction)
        registers.F.toggleFlag(.HalfCarry, when: halfOverflow)
        registers.F.toggleFlag(.Carry, when: overflowed)
        
        dest = result
    }
    
    func ADD_Register(_ dest:inout DWord, _ source: DWord) {
        let (result, overflowed) = dest.addingReportingOverflow(source)
        let hasHalfCarry = hasAdditionSetHalfCarry(lhs: dest, rhs: result)
        
        registers.F.toggleFlag(.Zero, when: result == 0)
        registers.F.resetFlag(.Subtraction)
        registers.F.toggleFlag(.HalfCarry, when: hasHalfCarry)
        registers.F.toggleFlag(.Carry, when: overflowed)
        
        dest = result
    }
    
    func SUB_Register(_ dest:inout Word, _ source: Word, withCarry: Bool = false) {
        var carry:Word = 0
        
        if (withCarry && registers.F.isFlagSet(.Carry)) {
            carry = 1
        }
        
        let (partial, overflowCarry) = dest.subtractingReportingOverflow(source)
        let (result, overflowSub) = partial.subtractingReportingOverflow(carry)
        let hasHalfCarry = hasSubtractionSetHalfCarry(lhs:dest, rhs:partial)
        let overflowed = overflowSub || overflowCarry
        
        registers.F.toggleFlag(.Zero, when: result == 0)
        registers.F.setFlag(.Subtraction)
        registers.F.toggleFlag(.HalfCarry, when: hasHalfCarry)
        registers.F.toggleFlag(.Carry, when: overflowed)

        dest = result
    }
    
    enum ShiftDirection { case left; case right }
    
    func Rotate_Shift_With_Carry(_ dest:inout Word, direction:ShiftDirection) {

        let bit0_mask:Word = 0b0000_0001
        let bit7_mask:Word = 0b1000_0000
        
        // Store carry bit
        let bitCarry:Word = registers.F.isFlagSet(.Carry) ? 0b0000 : 0b0001
        
        // Rotate left or right
        if(direction == ShiftDirection.right) {
            // Store bit leaving
            let bitLeaving:Word = dest & bit0_mask
            
            // Register is shifted right 1 bit
            dest >>= 1
            
            // clear bit 7
            dest |= bit7_mask
            
            // copy carry into bit 7
            dest |= bitCarry << 7

            // Save leaving bit as carry bit
            registers.F.toggleFlag(.Carry, when: bitLeaving == bit0_mask)

        } else {
            // Store bit leaving
            let bitLeaving:Word = dest & bit7_mask
            
            // Register is shifted left 1 bit
            dest <<= 1
            
            // clear bit 0
            dest |= bit0_mask
            
            // copy carry into bit 0
            dest |= bitCarry >> 7
            
            // Set bit leaving as carry bit
            registers.F.toggleFlag(.Carry, when: bitLeaving == bit7_mask)
        }
        
        registers.F.resetFlag(.Zero)
        registers.F.resetFlag(.Subtraction)
        registers.F.resetFlag(.HalfCarry)
    }
    
    func Rotate_Shift(_ dest:inout Word, direction:ShiftDirection) {
        let bit0_mask:Word = 0b0000_0001
        let bit7_mask:Word = 0b1000_0000

        if (direction == ShiftDirection.right) {
            let bitLeaving:Word = dest & bit0_mask
            // Shift right and copy the lsb into msb
            dest >>= 1
            
            dest |= bit7_mask
            dest |= bitLeaving << 7
            
            registers.F.toggleFlag(.Carry, when: bitLeaving == bit0_mask)
        } else {
            let bitLeaving:Word = dest & bit7_mask
            // Shift left and copy the msb into lsb
            dest <<= 1
            
            dest |= bit0_mask
            dest |= bitLeaving >> 7
            
            registers.F.toggleFlag(.Carry, when: bitLeaving == bit7_mask)
        }
        
        registers.F.toggleFlag(.Zero, when: dest == 0x00)
        registers.F.resetFlag(.Subtraction)
        registers.F.resetFlag(.HalfCarry)
    }
    
    func shift_into_carry_preserve(_ dest:inout Word, direction:ShiftDirection) {
        /*
         SRA n: Shift n right into Carry. MSB doesn't change.
         There is no SLL/SL1 instruction in the GB
        */
        if (direction == .right) {
            let MSB_copy = (dest & 0b1000_0000)
            shift_into_carry(&dest, direction: .right)
            dest |= MSB_copy
        }
    }
    
    func shift_into_carry(_ dest:inout Word, direction:ShiftDirection) {
        /*
         This function implements two different shift operations:
         SRL n: Shift n right into Carry. MSB of n set to 0
         SLA n: Shift n left into Carry. LSB of n set to 0.
         */
        
        let bit0_mask:Word = 0b0000_0001
        let bit7_mask:Word = 0b1000_0000
        
        var leavingBit:Word
        
        if (direction == .right) {
            leavingBit = (dest & bit0_mask)
            dest >>= 1
            
        } else {
            // No need to preserve the LSB when shifting left
            leavingBit = (dest & bit7_mask) >> 7
            dest <<= 1
        }
        
        // copy leaving bit to carry
        registers.F.toggleFlag(.Carry, when: leavingBit == 0x01)
        
        registers.F.toggleFlag(.Zero, when: dest == 0x00)
        registers.F.resetFlag(.Subtraction)
        registers.F.resetFlag(.HalfCarry)
    }
    
    func test_bit(_ bit:BitNumber, on value:Word) {
        let isSet = (value & bit.rawValue) == bit.rawValue
        
        registers.F.toggleFlag(.Zero, when: isSet)
        registers.F.setFlag(.HalfCarry)
        registers.F.resetFlag(.Subtraction)
    }
    
    func set_bit(_ bit:BitNumber, on value:inout Word) {
        value |= bit.rawValue
    }
    
    func reset_bit(_ bit:BitNumber, on value: inout Word) {
        value &= ~bit.rawValue
    }
    
    func swap(_ dest:inout Word) {
        let high_nibble = dest & 0b1111_0000
        dest <<= 4
        dest |= high_nibble >> 4
    }
    
    func Add_AsSigned(unsignedOperand: DWord, signedOperand: Word) -> DWord {
        let asSigned = Int16(Int8(truncatingIfNeeded: signedOperand))
        
        return DWord(truncatingIfNeeded: Int16(unsignedOperand) + asSigned)
    }
    
    func JUMP_With_Relative_Increment() {
        let PC_increment = Read_Word_From_PC()
        
        registers.PC = Add_AsSigned(unsignedOperand: registers.PC, signedOperand: PC_increment)
    }
    
    func JUMP_To_Address_Inmmediate() {
        let newAddress = Read_DWord_From_PC()
        registers.PC = newAddress
    }
    
    func AND_Register(_ dest:inout Word, _ source:Word) {
        
        let result = dest & source
        
        registers.F.toggleFlag(.Zero, when: result == 0)
        registers.F.setFlag(.HalfCarry)
        registers.F.resetFlag(.Subtraction)
        registers.F.resetFlag(.Carry)
        
        dest = result
    }
    
    func XOR_Register(_ dest:inout Word, _ source:Word) {
        
        let result = dest ^ source
        
        registers.F.toggleFlag(.Zero, when: result == 0)
        registers.F.resetFlag(.HalfCarry)
        registers.F.resetFlag(.Subtraction)
        registers.F.resetFlag(.Carry)
        
        dest = result
    }

    func OR_Register(_ dest:inout Word, _ source:Word) {
        
        let result = dest | source
        
        registers.F.toggleFlag(.Zero, when: result == 0)
        registers.F.resetFlag(.HalfCarry)
        registers.F.resetFlag(.Subtraction)
        registers.F.resetFlag(.Carry)
        
        dest = result
    }

    /*
        Compares the two values and sets the Zero flag if equal
        - This operation sets the Subtraction flag
        - This operation may set the Half Carry and Carry flags
     
     */
    func COMPARE_Registers(_ reg1: Word, _ reg2:Word) {
        // Apparently the CP opcode is implemented by subtracting both registers,
        // but without modifying the destination registry.
        
        var copy = reg1
        SUB_Register(&copy, reg2)
    }

    func PUSH_To_Stack(_ value: DWord) {
        let Words = value.extractHighLowWords()

        write(toAddress: registers.SP - 1, value: Words.high)
        write(toAddress: registers.SP - 2, value: Words.low)

        registers.SP -= 2
    }

    func POP_From_Stack() -> DWord {
        let ls_value = read(fromAddress: registers.SP)
        let ms_value = read(fromAddress: registers.SP + 1)
        registers.SP += 2

        return DWord(highWord: ms_value, lowWord: ls_value)
    }

    func CALL_Jump() {

        let newAddress = Read_DWord_From_PC()

        PUSH_To_Stack(registers.PC)

        registers.PC = newAddress
    }
    
    func RET_From_Call() {
        registers.PC = POP_From_Stack()
    }
    
    func RST_to_page_zero_address(_ lowWord:Word) {
        PUSH_To_Stack(registers.PC)
        let jumpAddress = DWord(highWord: 0x00, lowWord: lowWord)
        registers.PC = jumpAddress
    }
    
    /*
     Checks if an addition had a carry from the lower to the higher nibble
     - return: true if the half-carry bit should be set, false otherwise
     - parameters:
     - lhs: absolute value of left side operand
     - rhs: absolute valuel of right side operand
     */
    func hasAdditionSetHalfCarry(lhs: Word, rhs: Word) -> Bool {
        return ((lhs & 0xF) + (rhs & 0xF) & 0x10) == 0x10
    }
    
    /*
     Checks if a subtraction had a borrow from the lower to the higher nibble
     - return: true if the half-carry bit should be set, false otherwise
     - parameters:
     - lhs: absolute value of left side operand
     - rhs: absolute valuel of right side operand
     */
    func hasSubtractionSetHalfCarry(lhs: Word, rhs: Word) -> Bool {
        return (lhs & 0xF) < (rhs & 0xF)
    }
    
    /*
     Checks if an addition had a carry from the lower to the higher nibble
     in a 16-bit operation
     
     - return: true if the half-carry bit should be set, false otherwise
     - parameters:
     - lhs: absolute value of left side operand
     - rhs: absolute valuel of right side operand
     
     
     - Important: in a 16-bit operation a half-carry happens if there is a
     carry from bit 11 to 12
     */
    func hasAdditionSetHalfCarry(lhs: DWord, rhs: DWord) -> Bool {
        return ((lhs & 0xFFF) + (rhs & 0xFFF) & 0x1000) == 0x1000
    }
}
