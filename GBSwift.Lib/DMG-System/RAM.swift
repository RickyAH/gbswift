//
//  Memory.swift
//  GBSwift.Lib
//
//  Created by Ricardo Amores Hernández on 5/8/18.
//  Copyright © 2018 develovers. All rights reserved.
//

public class RAM : SequencialAccessDevice {
    
    final var _data:UnsafeFixedArray<Word>
    
    final public var count:Int {
        get {
            return _data.count
        }
    }
    
    public convenience init(count:Int) {
        self.init(withContents: Array.init(repeating: Word(), count: count))
    }
    
    public init(withContents contents:[Word]) {
        _data = UnsafeFixedArray<Word>(withContents: contents)
    }
    
    public func read(atIndex: Int) -> Word {
        return _data[atIndex]
    }
    
    public func write(atIndex: Int, value: Word) {
        _data[atIndex] = value
    }
}

