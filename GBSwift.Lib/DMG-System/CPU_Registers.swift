//
//  Registers.swift
//  GBSwift
//
//  Created by Ricardo Amores Hernández on 31/5/18.
//  Copyright © 2018 develovers. All rights reserved.

extension CPU {
    public struct CPU_Registers : CustomStringConvertible {
        public enum Flags : Word {
            case Zero           = 0b1000_0000
            case Subtraction    = 0b0100_0000
            case HalfCarry      = 0b0010_0000
            case Carry          = 0b0001_0000
        }
        
        struct FlagsRegistry : CustomStringConvertible {
            var value:Word = 0x00
            
            public mutating func setFlag(_ flag:Flags) {
                self.value |= flag.rawValue
            }
            
            public mutating func resetFlag(_ flag:Flags) {
                value &= ~flag.rawValue
            }
            
            public func isFlagSet(_ flag:Flags) -> Bool {
                return (value & flag.rawValue == flag.rawValue)
            }
            
            public mutating func toggleFlag(_ flag:Flags, when shouldSet:Bool) {
                if (shouldSet) {
                    setFlag(flag)
                } else {
                    resetFlag(flag)
                }
            }
            
            var description: String {
                get {
                    return "\(Flags.Zero): \(isFlagSet(Flags.Zero)) \(Flags.Subtraction): \(isFlagSet(Flags.Subtraction)) \(Flags.HalfCarry): \(isFlagSet(Flags.HalfCarry)) \(Flags.Carry): \(isFlagSet(Flags.Carry)) "
                }
            }
        }

        public init() {
            self.reset()
        }
        
        public mutating func reset() {
            A = 0x11
            B = 0x00
            C = 0x00
            D = 0xFF
            E = 0x56
            H = 0x00
            L = 0x0D
            
            SP = 0xFFFE
            PC = 0x0100
            
            F.value = 0x80
        }

        // Flags register does not need to allow public access as the only way to change it is changing the specific flags
        // There is one exception when used as part of the AF construct throug the PUSH AF, POP AF opcodes
        var F = FlagsRegistry()
        
        public var A:Word = 0 // Accumulator register
        public var SP:DWord = 0 // Stack Pointer
        public var PC:DWord = 0 // Program Counter
        
        // General purpose registers
        public var B:Word = 0
        public var C:Word = 0
        public var D:Word = 0
        public var E:Word = 0
        public var H:Word = 0
        public var L:Word = 0
        
        // Double Word registers
        public var AF:DWord {
            get { return DWord(highWord:A, lowWord:F.value) }
            // Lower nibble of the flag register should always be zero so we force it
            // when writing into the register
            set (value) {  (A, F.value) = value.extractHighLowWords() }
        }
        
        public var BC:DWord {
            get { return DWord(highWord:B, lowWord:C) }
            set (value) { B = Word(value >> 8); C = Word(value & 0x00FF) }
        }
        
        public var DE:DWord {
            get { return DWord(highWord:D, lowWord:E) }
            set (value) { (D ,E) = value.extractHighLowWords() }
        }
        
        public var HL:DWord {
            get { return DWord(highWord:H, lowWord:L) }
            set (value) { (H, L) = value.extractHighLowWords()}
        }
       
        public var description: String {
            get {
                return "[Registers]\nPC: \(PC.asHex())\nSP: \(SP.asHex())\nA: \(A.asHex())\nF: \(F)\nB: \(B.asHex())\nC: \(C.asHex())\nD: \(D.asHex())\nE: \(E.asHex())\nH: \(H.asHex())\nL: \(L.asHex())\nAF: \(AF.asHex())\nBC: \(BC.asHex())\nDE: \(DE.asHex())\nHL:\(HL.asHex())"
            }
        }
    }
}
