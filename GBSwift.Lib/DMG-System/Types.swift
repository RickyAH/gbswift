//
//  Types.swift
//  GBSwift.Lib
//
//  Created by Ricardo Amores Hernández on 12/8/18.
//  Copyright © 2018 develovers. All rights reserved.
//

public typealias Word = UInt8
public typealias DWord = UInt16
public typealias Address = DWord

public enum BitNumber: Word {
    case b0 = 0b0000_0001
    case b1 = 0b0000_0010
    case b2 = 0b0000_0100
    case b3 = 0b0000_1000
    case b4 = 0b0001_0000
    case b5 = 0b0010_0000
    case b6 = 0b0100_0000
    case b7 = 0b1000_0000
}

extension Word {
    
    func getBit(_ bit:BitNumber) -> Bool {
        return self & bit.rawValue == bit.rawValue
    }
    
    mutating func setBit(_ bit:BitNumber) {
        self |= bit.rawValue
    }
    
    mutating func resetBit(_ bit:BitNumber) {
        self &= ~bit.rawValue
    }
}

// This will allow to easily print the value of a Word, DWord or Address in hexadecimal

import Foundation
extension Word {
    public func asHex() -> String {
        return String(format:"%02X", self as CVarArg)
    }
}

extension DWord {
    public func asHex() -> String {
        return String(format:"%04X", self as CVarArg)
    }
}

extension DWord {
    init (highWord:Word, lowWord:Word) {
        self.init((DWord(highWord) << Word.bitWidth) | DWord(lowWord))
    }
    
    func extractHighLowWords() -> (high:Word, low:Word) {
        let highWord = Word((self & 0xFF00) >> Word.bitWidth)
        let lowWord = Word(self & 0x00FF)
        
        return (highWord, lowWord)
    }
}


