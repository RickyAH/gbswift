//
//  Cartdridge.swift
//  GBSwift.Lib
//
//  Created by Ricardo Amores Hernández on 5/8/18.
//  Copyright © 2018 develovers. All rights reserved.
//

public class Cartdridge {
    public var ROMData:ROM
    public var SRAMData:RAM?

    public init(withROMData romData:ROM, andSRAMData sramData:RAM?) {
        ROMData = romData
        
        if sramData != nil {
            SRAMData = sramData
        }
    }
}
