//
//  InterruptController.swift
//  GBSwift.Lib
//
//  Created by Ricardo Amores Hernández on 14/8/18.
//  Copyright © 2018 develovers. All rights reserved.
//

public class InterruptController {
    public enum Interrupt: Word, CaseIterable {
        case VBlank     = 0b0000_0001
        case LCD_Stat   = 0b0000_0010
        case Timer      = 0b0000_0100
        case Serial     = 0b0000_1000
        case Joypad     = 0b0001_0000
        case None       = 0b0000_0000
    }
    
    var areInterruptionsEnabled = true
    var interruptFlag:HardwareRegister
    var interruptEnable:HardwareRegister
    var cpu:CPU
    
    init(withCPU cpu:CPU) {
        self.cpu = cpu
        
        let allInterruptsMask:Word = Interrupt.allCases.reduce(0) { (result, element) in
            result | element.rawValue
        }
        
        interruptFlag = HardwareRegister(withInitialValue: 0x00,
                                         accesibleBitsBitMask: allInterruptsMask,
                                         defaultValueBitMask: 0x00)
        
        interruptEnable = HardwareRegister(withInitialValue: allInterruptsMask,
                                           accesibleBitsBitMask: allInterruptsMask,
                                           defaultValueBitMask: allInterruptsMask)
    }
    
    var hasPendingInterruptions : Bool {
        get {
            if !areInterruptionsEnabled {
                return false
            }
            
            guard let interrupt = getNextActiveInterrupt1() else {
                return false
            }
            
            return isInterruptEnabled(interrupt)
        }
    }

    func findActiveInterrupt1() -> Interrupt? {
        
        if !areInterruptionsEnabled { return nil }
        
        guard let activeInterrupt = getNextActiveInterrupt1() else {
            return nil;
        }
        
        if (!isInterruptEnabled(activeInterrupt)) { return nil }
        
        return activeInterrupt
    }
     
    func enableInterruptions() {
        areInterruptionsEnabled = true
    }
    
    func disableInterruptions() {
        areInterruptionsEnabled = false
    }
    
    func activateInterrupt(_ int:Interrupt) {
        // The IF can be set even when the interrupt is disabled
        interruptFlag.value |= int.rawValue
    }
    
    func deactivateInterrupt(_ int:Interrupt) {
        // The IF can be reset even when the interrupt is disabled
        interruptFlag.value &= ~int.rawValue
    }
    
    func getNextActiveInterrupt() -> Interrupt {
        
        // This is the interrupt priority order for DMG
        if isInterruptActive(.VBlank)   { return .VBlank }
        if isInterruptActive(.LCD_Stat) { return .LCD_Stat }
        if isInterruptActive(.Timer)    { return .Timer }
        if isInterruptActive(.Serial)   { return .Serial }
        if isInterruptActive(.Joypad)   { return .Joypad }
        
        return .None
    }
    
    func getNextActiveInterrupt1() -> Interrupt? {

        // This is the interrupt priority order for DMG
        if isInterruptActive(.VBlank)   { return .VBlank }
        if isInterruptActive(.LCD_Stat) { return .LCD_Stat }
        if isInterruptActive(.Timer)    { return .Timer }
        if isInterruptActive(.Serial)   { return .Serial }
        if isInterruptActive(.Joypad)   { return .Joypad }
        
        return nil
    }
    
    func isInterruptEnabled(_ int:Interrupt) -> Bool {
        return interruptEnable.value & int.rawValue == int.rawValue
    }
    
    func isInterruptActive(_ int:Interrupt) -> Bool {
        return interruptFlag.value & int.rawValue == int.rawValue
    }
    
    static func getJumpAddressFor(_ interrupt:Interrupt) -> Address {
        switch(interrupt) {
        case .VBlank:
            return 0x0040
        case .LCD_Stat:
            return 0x0048
        case .Timer:
            return 0x0050
        case .Serial:
            return 0x0058
        case .Joypad:
            return 0x0060
        case .None:
            fatalError("Interrupt.None is not a valid interrupt")
        }
    }

}
