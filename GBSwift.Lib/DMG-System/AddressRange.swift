//
//  AddressRange.swift
//  GBSwift.Lib
//
//  Created by Ricardo Amores Hernández on 10/8/18.
//  Copyright © 2018 develovers. All rights reserved.
//
import Foundation


public struct AddressRange {
    public let upperBound:Address
    public let lowerBound:Address
    public let count:Int
    
    public init(_ range: ClosedRange<Address>) {
        self.upperBound = range.upperBound
        self.lowerBound = range.lowerBound
        self.count = range.count
    }
    
    public init(_ range: Range<Address>) {
        self.upperBound = range.upperBound-1
        self.lowerBound = range.lowerBound
        self.count = range.count
    }
    
    public func contains(_ element: Address) -> Bool {
        return (element >= lowerBound && element <= upperBound)
    }
    
    public func getZeroBasedIndex(forAddress address:Address) -> Int {
        return Int(address - lowerBound)
    }
}

extension AddressRange : Comparable {
    public static func ==(left: AddressRange, right: AddressRange) -> Bool {
        return left.lowerBound == right.lowerBound
            && left.upperBound == right.upperBound
    }
    
    public static func <(left: AddressRange, right: AddressRange) -> Bool {
        return left.lowerBound < right.lowerBound
    }
    
    public static func >(left: AddressRange, right: AddressRange) -> Bool {
        return left.upperBound > right.upperBound
    }
}

extension AddressRange : CustomStringConvertible {
    public var description: String {
        get {
            return "[\(lowerBound.asHex()), \(upperBound.asHex())]"
        }
    }
}
