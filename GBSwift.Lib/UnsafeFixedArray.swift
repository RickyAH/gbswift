//
//  UnsafeFixedArray.swift
//  GBSwift.Lib
//
//  Created by Ricardo Amores Hernández on 25/11/2018.
//  Copyright © 2018 develovers. All rights reserved.
//

public struct UnsafeFixedArray<T: FixedWidthInteger> {
    let _data:UnsafeMutableRawPointer
    public let count:Int
    
    public  init(count:Int) {
        self.init(withContents: Array.init(repeating: T(), count: count))
    }
    
    public init(withContents contents:[T]) {
        _data = UnsafeMutableRawPointer.allocate(byteCount: contents.count, alignment: T.bitWidth/8)
        count = contents.count
        
        for index in 0..<self.count {
            _data.storeBytes(of: contents[index], toByteOffset: index, as: T.self)
        }
    }
    
    subscript(index: Int) -> T {
        get {
            return self.load(fromByteOffset: index, as: T.self)
        }
        set(newValue) {
            self.storeBytes(of: newValue, toByteOffset: index, as: T.self)
        }
    }
    
    func load(fromByteOffset offset: Int, as type: T.Type) -> T {
        return _data.load(fromByteOffset: offset, as: T.self)
    }
    
    func storeBytes<T>(of value: T, toByteOffset offset: Int, as type: T.Type) {
        _data.storeBytes(of: value, toByteOffset: offset, as: T.self)
    }
}
