//
//  TimeUtils.swift
//  GBSwift.Lib
//
//  Created by Ricardo Amores Hernández on 26/11/2018.
//  Copyright © 2018 develovers. All rights reserved.
//

import Foundation

public struct Time {
    public static func getTimeInMs() -> Double {
        var info = mach_timebase_info()
        guard mach_timebase_info(&info) == KERN_SUCCESS else { return 0 }
        let currentTime = mach_absolute_time()
        let nanos = Double(currentTime * UInt64(info.numer) / UInt64(info.denom))
        
        return nanos / 1e+6
    }
    
}
